var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');
var autoprefixer = require('gulp-autoprefixer');
var fileinclude = require('gulp-file-include');


gulp.task('css', function(){
    return gulp.src('src/scss/style.scss')
        .pipe(
            sass().on('error', sass.logError)
        )
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('js', function(){
   return gulp.src('src/js/**/*.js')
       .pipe(browserSync.reload({
           stream: true
       }))
});

gulp.task('watch', ['browserSync', 'html', 'css'], function (){
    gulp.watch('src/scss/**/*.scss', ['css']);
    gulp.watch('src/**/*.html', ['html']);
    gulp.watch('src/**/*.js', ['js']);
});

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: 'src'
        }
    })
});

gulp.task('html', function(){
    return gulp.src('src/pages/*.html')
        .pipe(fileinclude())
        .pipe(gulp.dest('src'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('concat', function(){
    return gulp.src('src/*.html')
        .pipe(useref())
        .pipe(gulpIf('*.js', uglify()))
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest('build'))
});

gulp.task('images', function(){
    return gulp.src('src/images/**/*.+(png|jpg|jpeg|gif|svg)')
        .pipe(cache(imagemin({
            interlaced: true
        })))
        .pipe(gulp.dest('build/images'))
});

gulp.task('fonts', function() {
    return gulp.src('src/fonts/**/*')
        .pipe(gulp.dest('build/fonts'))
});

gulp.task('clean', function() {
    return del.sync('build');
});

gulp.task('clear:cache', function (callback) {
    return cache.clearAll(callback)
});

gulp.task('build', function (callback) {
    runSequence('clean',
        ['html', 'css', 'concat', 'images', 'fonts'],
        callback
    )
});

gulp.task('default', function (callback) {
    runSequence(['html', 'css', 'browserSync', 'watch'],
        callback
    )
});